package com.sage.pojos;

public class AuthToken {
    private String grantType;
    private String accessTokenUrl;
    private String username;
    private String password;
    private String clientId;
    private String clientSecret;
    private String scope;
    private String clientAuthentication;

    AuthToken(String grantType, String accessTokenUrl, String username, String password,
              String clientId, String clientSecret, String scope, String clientAuthentication) {
        this.grantType = grantType;
        this.accessTokenUrl = accessTokenUrl;
        this.username = username;
        this.password = password;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.scope = scope;
        this.clientAuthentication = clientAuthentication;
    }



    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getAccessTokenUrl() {
        return accessTokenUrl;
    }

    public void setAccessTokenUrl(String accessTokenUrl) {
        this.accessTokenUrl = accessTokenUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getClientAuthentication() {
        return clientAuthentication;
    }

    public void setClientAuthentication(String clientAuthentication) {
        this.clientAuthentication = clientAuthentication;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


}

package com.sage.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.support.ui.Select;

public class WebExtensibility extends PageObject {

	public WebDriver driver;
	String parentWindow;

	public WebDriver launchBrowser(String url) {
		WebDriverManager.chromedriver().setup();
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--incognito");
		driver = new ChromeDriver(chromeOptions);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		parentWindow = driver.getWindowHandle();
		setDriver(driver);
		return driver;
	}

	public void waitForVisibleTextToAppear(WebElement element) {
		waitForCondition().until(
				ExpectedConditions.elementToBeClickable(element));
	}

	public void selectValueFromDropdown(String element, String text) {
		Select primaryUsageOfProperty = new Select(getDriver().
				findElement(By.xpath(element)));
     	primaryUsageOfProperty.selectByVisibleText(text);
	}
}

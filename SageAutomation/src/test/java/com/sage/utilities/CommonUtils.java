package com.sage.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CommonUtils {

	public static String getRandomString(int number) {
		String alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz";
		StringBuilder sb = new StringBuilder(number);
		for(int i=0; i < number; i++) {
			int index = (int) (alphaNumeric.length() * Math.random());
			sb.append(alphaNumeric.charAt(index));
		}
		return sb.toString();
	}
	
	public static String getTheRequiredDate(String pattern) {
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		String date = dateFormat.format(new Date(calendar.getTimeInMillis()));
		return date;
	}
}

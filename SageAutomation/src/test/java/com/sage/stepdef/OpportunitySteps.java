package com.sage.stepdef;

import com.sage.utilities.DataHelper;
import org.apache.log4j.Logger;

import com.sage.actors.OpportunityActors;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import net.thucydides.core.annotations.Steps;
import org.yecht.Data;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class OpportunitySteps {

    Logger log = Logger.getLogger(OpportunitySteps.class);
    public static String testDataFilePath = System.getProperty("user.dir") + File.separator + "src" + File.separator
            + "test" + File.separator + "resources" + File.separator + "testData"
            + File.separator + "AddOpportunity.xlsx";
    List<HashMap<String, String>> datamap;
    DataHelper dataHelper = new DataHelper();

    @Steps
    OpportunityActors opportunityActors;

    @Given("^user logs into sage application$")
    public void launch_the_application_and_enter_email() {
        opportunityActors.loginapplication();
    }

    @Then("^validate the login scenario$")
    public void validate_the_login_scenario() {
        Assert.assertTrue("True", true);
    }

    @Given("^closing the browser$")
    public void closing_the_browser() {
        opportunityActors.closeBrowser();
    }

    @Given("^verify the sales pipeline link$")
    public void verify_sales_pipeline() {
        opportunityActors.salespipelinelink();
    }

    @Then("^click on sales pipeline link$")
    public void click_on_sales_pipeline_link() {
        opportunityActors.clickOnSalesPipelineLink();
    }

    @And("^verify insurance is in selected state by default$")
    public void verify_person_tab_state() {
        opportunityActors.verifyPersontabState();
    }

//	@Given("^filling the opportunity form (.+), (.+), (.+), (.+), (.+), (.+) (.+)$")
//	public void filling_the_opportunity_form(String fname, String lname, String phone, String email, String address,
//			String addPolicies, String sourceName) {
//		data = dataHelper.data(testDataFilePath, "Sheet1");
//		opportunityActors.fillTheOpportunityForm(fname, lname, phone, email, address, addPolicies, sourceName);
//	}

    @Given("^filling the opportunity form$")
    public void filling_the_opportunity_form() {
        datamap = dataHelper.data(testDataFilePath, "Personal");
        for (int i = 1; i < datamap.size(); i++) {
            log.info("Printing current data set...");
            for (HashMap h : datamap) {
                log.info(h.keySet());
                log.info(h.values());
            }
            String insuranceType = datamap.get(i).get("Insurance Type");
            String fname = datamap.get(i).get("First Name");
            String lname = datamap.get(i).get("Last Name");
            String phone = datamap.get(i).get("Phone Number");
            String email = datamap.get(i).get("Email");
            String address = datamap.get(i).get("Address");
            String addPolicies = datamap.get(i).get("Add Policies");
            String sourceName = datamap.get(i).get("Source Name");
            opportunityActors.fillTheOpportunityPersonalForm(insuranceType, fname, lname, phone, email, address, addPolicies, sourceName);
        }
    }

    @Given("^filling the opportunity form for business$")
    public void filling_the_opportunity_form_for_business() {
        datamap = dataHelper.data(testDataFilePath, "Business");
        for (int i = 1; i < datamap.size(); i++) {
            log.info("Printing current data set...");
            for (HashMap h : datamap) {
                log.info(h.keySet());
                log.info(h.values());
            }
            String insuranceType = datamap.get(i).get("Insurance Type");
            String fname = datamap.get(i).get("First Name");
            String lname = datamap.get(i).get("Last Name");
            String phone = datamap.get(i).get("Phone Number");
            String email = datamap.get(i).get("Email");
            String address = datamap.get(i).get("Address");
            String addPolicies = datamap.get(i).get("Add Policies");
            String sourceName = datamap.get(i).get("Source Name");
            String relationshipToBusiness = datamap.get(i).get("Relationship");
            String businessName = datamap.get(i).get("Business");
            String industry = datamap.get(i).get("Industry");
            opportunityActors.fillTheOpportunityBusinessForm(insuranceType, fname, lname, phone, email, relationshipToBusiness,
                        businessName, address, industry, addPolicies,  sourceName);
        }
    }

    @And("^filling the quoting form$")
    public void start_quoting() {
        opportunityActors.startQuoting();
    }

}

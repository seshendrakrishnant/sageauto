package com.sage.pages;

import com.sage.utilities.WebExtensibility;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class QuotingPage extends WebExtensibility{

    @FindBy(className = "css-1xkqfcl")
    WebElement quotingButton;

    @FindBy(css = "input[name='SAGEPLHOME0011']")
    WebElement propertyDetailsTxt;

    @FindBy(css = "input[id='SAGEPLHOME0012']")
    WebElement constructionYearTxt;

    @FindBy(css = "input[id='SAGEPLHOME0013']")
    WebElement squareFeetTxt;

    @FindBy(xpath = "//input[@id='downshift-5-input']")
    WebElement primaryUseOfProperty;

    @FindBy(css = "input[id='downshift-6-input']")
    WebElement numberOfStories;

    @FindBy( xpath = "//div[@class='css-1s98nfo']//button[@class='css-ozpa5g']")
    WebElement propertyContinueBtn;

    @FindBy(css = "input[name='SAGEPLHOME0017']")
    WebElement propertyConstruction;

    @FindBy(css = "input[name='SAGEPLHOME0019']")
    WebElement constructionStyle;

    @FindBy(css = "input[name='SAGEPLHOME0020']")
    WebElement foundationType;

    @FindBy(css= "input[name='SAGEPLHOME0021']")
    WebElement exteriorWallMaterial;

    @FindBy(css = "input[name='SAGEPLHOME0022']")
    WebElement roofType;

    @FindBy(css = "input[name='SAGEPLHOME0024']")
    WebElement roofShape;

    @FindBy(css = "input[name='SAGEPLHOME0025']")
    WebElement noOfFullBath;

    @FindBy(css = "input[name='SAGEPLHOME0026']")
    WebElement noOfHalfBath;

    @FindBy(css = "input[name='SAGEPLHOME0027']")
    WebElement threeByFourthBath;

    @FindBy(css = "//div[@class='css-k008qs']//div[@class='css-1sshroq']/input[@class='css-1kgwrz0']")
    List<WebElement> yearsAndMonths;

    @FindBy(css = "input[name='SAGEPLHOME0029']")
    WebElement purchasePrice;

    @FindBy(css = "input[name='SAGEPLHOME0031']")
    WebElement primaryHeating;

    @FindBy(css = "input[name='SAGEPLHOME0032']")
    WebElement secondaryHeating;

    @FindBy(css = "input[name='SAGEPLHOME0033']")
    WebElement garage;

    @FindBy(css = "input[name='SAGEPLHOME0036']")
    WebElement noOfFirePlaces;

    @FindBy(css = "input[name='SAGEPLHOME0038']")
    WebElement typeOfTorch;

    @FindBy(css = "input[name='SAGEPLHOME0039']")
    WebElement roofingUpdateType;

    @FindBy(css = "input[name='SAGEPLHOME0041']")
    WebElement heatingSystemUpdate;

    @FindBy(css = "input[name='SAGEPLHOME0043']")
    WebElement electricSystemUpdate;

    @FindBy(css = "input[name='SAGEPLHOME0045']")
    WebElement plumbingUpdate;

    @FindBy(css = "input[name='SAGEPLHOME0047']")
    WebElement milesUpdate;

    @FindBy(css = "input[name='SAGEPLHOME0048']")
    WebElement feetToClosest;

    @FindBy(css = "//div[@id='SAGEPLHOME0050']/button[@name='SAGEPLHOME0050']")
    List<WebElement> yesNoButton;

    @FindBy(css = "input[name='SAGEPLHOME0051']")
    WebElement publicProtectionClass;

    @FindBy(xpath = "//div[@class='css-1s98nfo']//button[@class='css-ozpa5g']")
    WebElement dwellingContinuewBtn;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0052']")
    List<WebElement> constructionYesNo;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0053']")
    List<WebElement> lastThreeYearYesNo;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0054']")
    List<WebElement> burningStove;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0055']")
    List<WebElement> swimmingPool;

    @FindBy(xpath = "//input[@name='SAGEPLHOME0058']")
    WebElement hotTub;

    @FindBy(xpath = "//input[@name='SAGEPLHOME0060']")
    WebElement typeOFAnimalONTheProperty;

    @FindBy(xpath = "//input[@name='SAGEPLHOME0066']")
    WebElement floodZone;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0067']")
    List<WebElement> FEMAPolicyYesNo;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0059']")
    List<WebElement> trumpoLineYesNo;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0073']")
    List<WebElement> propertyLossClaimYesNo;

    @FindBy(xpath = "//button[@name='SAGEPLHOME0069']")
    List<WebElement> priorInsuranceYesNo;

    @FindBy(xpath = "//div[@class='css-1s98nfo']//button[@class='css-ozpa5g']")
    WebElement additionalContinueButton;

    @FindBy(css = "input[name='SAGEPLHOME0082']")
    WebElement barglarAlram;

    @FindBy(css = "input[name='SAGEPLHOME0083']")
    WebElement fireAlram;

    @FindBy(css = "input[name='SAGEPLHOME0084']")
    WebElement sprinklerSystem;

    @FindBy(css = "input[name='SAGEPLHOME0085']")
    WebElement gatedCommunity;

    @FindBy(css = "input[name='SAGEPLHOME0106']")
    WebElement coverageA;

    @FindBy(css = "input[name='SAGEPLHOME0107']")
    WebElement coverageB;

    @FindBy(css = "input[name='SAGEPLHOME0108']")
    WebElement coverageC;

    @FindBy(css = "input[name='SAGEPLHOME0109']")
    WebElement coverageD;

    @FindBy(css = "input[name='SAGEPLHOME0110']")
    WebElement coverageE;

    @FindBy(css = "input[name='SAGEPLHOME0111']")
    WebElement coverageF;

    @FindBy(css = "input[name='SAGEPLHOME0113']")
    WebElement allPerlisDeductable;

    @FindBy(css = "input[name='SAGEPLHOME0114']")
    WebElement windDeductable;

    @FindBy(css = "input[name='SAGEPLHOME0125']")
    WebElement DOB;

    @FindBy(css = "input[name='SAGEPLHOME0126']")
    WebElement gender;

    @FindBy(css = "input[name='SAGEPLHOME0127']")
    WebElement maritalStatus;

    @FindBy(css = "input[name='SAGEPLHOME0128']")
    WebElement socialSecurityNumber;

    @FindBy(css ="input[name='SAGEPLHOME0132']")
    WebElement mailingAddress;

    @FindBy(css = "button[class='css-ojwlkn']")
    WebElement quoteSubmisstionBtn;

    public void startFillingQuotingForm() {
        waitForVisibleTextToAppear(quotingButton);
        quotingButton.click();
        waitABit(30000);
        propertyDetailsTxt.clear();
        propertyDetailsTxt.sendKeys("481 Dollar Mill Rd SW, Atlanta, GA 30331");
        constructionYearTxt.clear();
        constructionYearTxt.sendKeys("1985");
        squareFeetTxt.clear();
        squareFeetTxt.sendKeys("1691");
        primaryUseOfProperty.sendKeys("Owner Occupied - Secondary");
        numberOfStories.sendKeys("1 Story");
        waitABit(60000);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0,1000)");
        js.executeScript("document.getElementsByClassName('css-ozpa5g').item(0).click();");
        waitForVisibleTextToAppear(propertyConstruction);
        propertyConstruction.sendKeys("Superior");
        constructionStyle.sendKeys("Cottage");
        foundationType.sendKeys("Basement-Finished");
        exteriorWallMaterial.sendKeys("Concrete Block");
        roofType.sendKeys("Composite Shingle");
        roofShape.sendKeys("Flat");
        noOfFullBath.clear();
        noOfFullBath.sendKeys("2");
        noOfHalfBath.clear();
        noOfHalfBath.sendKeys("0");
        threeByFourthBath.clear();
        threeByFourthBath.sendKeys("0");
        js.executeScript("document.getElementsByClassName(\"css-k008qs\").item(0).children[0].lastElementChild.children[0].value='2'");
        js.executeScript("document.getElementsByClassName(\"css-k008qs\").item(0).children[1].lastElementChild.children[1].children[0].value='10'");
        purchasePrice.clear();
        purchasePrice.sendKeys("169800");
        primaryHeating.sendKeys("Central Electric");
        secondaryHeating.sendKeys("None");
        garage.sendKeys("Attached");
        noOfFirePlaces.sendKeys("2");
        typeOfTorch.sendKeys("Embedded");
        roofingUpdateType.sendKeys("None");
        heatingSystemUpdate.sendKeys("None");
        electricSystemUpdate.sendKeys("None");
        plumbingUpdate.sendKeys("None");
        milesUpdate.sendKeys("10");
        feetToClosest.sendKeys("50");
        js.executeScript("document.getElementsByName('SAGEPLHOME0050').item(0).click();");
        publicProtectionClass.sendKeys("1");
        js.executeScript("document.getElementsByClassName(\"css-ozpa5g\").item(0).click();");
        waitABit(10000);
        hotTub.sendKeys("No");
        typeOFAnimalONTheProperty.sendKeys("None");
        floodZone.sendKeys("X");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0052\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0053\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0054\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0055\").item(1).click()");
        js.executeScript("document.getElementsByClassName(\"css-ozpa5g\").item(0).click();");
        waitABit(10000);

        barglarAlram.sendKeys("Central");
        fireAlram.sendKeys("Local");
        sprinklerSystem.sendKeys("Full");
        gatedCommunity.sendKeys("Not Gated");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0087\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0088\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0089\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0090\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0091\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0095\").item(1).click();");
        js.executeScript("document.getElementsByClassName(\"css-ozpa5g\").item(0).click();");
        waitABit(10000);

        coverageA.clear();
        coverageA.sendKeys("430000");
        coverageB.clear();
        coverageB.sendKeys("43000");
        coverageC.clear();
        coverageC.sendKeys("215000");
        coverageD.clear();
        coverageD.sendKeys("86000");
        coverageE.clear();
        coverageE.sendKeys("300000");
        coverageF.clear();
        coverageF.sendKeys("5000");
        allPerlisDeductable.sendKeys("1000");
        windDeductable.sendKeys("1000");

        js.executeScript("document.getElementsByName(\"SAGEPLHOME0115\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0116\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0117\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0118\").item(1).click();");
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0120\").item(1).click();");
        js.executeScript("document.getElementsByClassName('css-ozpa5g').item(0).click();");

        DOB.clear();
        DOB.sendKeys("03/03/1983");
        gender.click();
        gender.sendKeys("Male");
        maritalStatus.sendKeys("Single");
        socialSecurityNumber.clear();
        socialSecurityNumber.sendKeys("123456789");
        mailingAddress.clear();
        mailingAddress.sendKeys("test@test.com");;
        js.executeScript("document.getElementsByName(\"SAGEPLHOME0180\").item(1).click();");

        quoteSubmisstionBtn.click();
    }
}

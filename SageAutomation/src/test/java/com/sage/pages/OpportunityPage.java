package com.sage.pages;

import com.sage.utilities.PropertyReader;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static java.time.temporal.ChronoUnit.SECONDS;

import com.sage.utilities.CommonUtils;
import com.sage.utilities.WebExtensibility;

import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class OpportunityPage extends WebExtensibility {

    public static final String filename = System.getProperty("user.dir") + File.separator + "src" + File.separator
            + "test" + File.separator + "resources" + File.separator + "test_Env.properties";
    PropertyReader pReader = new PropertyReader();

    @FindBy(xpath = "//a[@title='Sales Pipeline']")
    WebElementFacade salesPipeline;

    @FindBy(css = "button.css-1raq1ke")
    WebElementFacade addOpportunity;

    @FindBy(css = "div.css-1x2z6hq div.css-16at8kc button:nth-of-type(1)")
    WebElementFacade insuranceTypePerson;

    @FindBy(css = "div.css-1x2z6hq div.css-16at8kc button:nth-of-type(2)")
    WebElementFacade insuranceTypeBusiness;

    @FindBy(css = "div[class='css-non77w'] div[class='css-1sshroq'] input[class='css-lylp3p']")
    WebElementFacade addPoliciesDropdown;

    @FindBy(css = "input#firstName")
    WebElementFacade firstName;

    @FindBy(css = "input#lastName")
    WebElementFacade lastName;

    @FindBy(css = "input[name='phoneNumber']")
    WebElementFacade phoneNumber;

    @FindBy(css = "input[name='email']")
    WebElementFacade emailId;

    @FindBy(css = "input[name='contactAddress']")
    WebElementFacade contactAddress;

    @FindBy(css = "input[name*='effectiveDate']")
    WebElementFacade effectiveDate;

    @FindBy(css = "input[name*='sourceType']")
    WebElementFacade sourceType;

    @FindBy(css = "div[class='css-3xsase'] ul li:nth-of-type(1)")
    WebElementFacade sourceTypelist;

    @FindBy(css = "input[name*='sourceName']")
    WebElementFacade sName;

    @FindBy(css = "button[class='css-1n66muu']")
    WebElementFacade submitBtn;

    @FindBy(css = "div.css-15g2oxy table.css-2tnh75 tr:nth-of-type(1) td:nth-of-type(7) div[class='css-1vss5ax'] button")
    WebElementFacade startBtn;

    @FindBy(xpath = "//ul[contains(@id,'downshift')]/li[1]")
    WebElement addressDropdownlst;

    @FindBy(css = "div[name='insuranceType'] div.css-16at8kc button:nth-of-type(2)")
    WebElement insuranceBusinessType;

    @FindBy(css = "input#relationshipToBusiness")
    WebElementFacade relationshipToBusinessTxt;

    @FindBy(css = "input[name='businessAddress']")
    WebElementFacade businessAddress;

    @FindBy(css = "input[name='businessName']")
    WebElementFacade businessNameTxt;

	@FindBy(css = "input[name='industry']")
    WebElementFacade industryTxt;

	@FindBy(css = "div[class='css-non77w'] div[class='css-1sshroq'] div[class='css-17gtvqy'] button[class='css-geq2st']")
    WebElementFacade addPoliciesdropdown;

	@FindBy(css = "ul[class='css-1xiqnhg']")
    WebElementFacade addPolicieslstForBusiness;

    String dropdownValues = "ul.css-1xiqnhg > li:nth-of-type(index)";
    String industryValue = "ul[class='css-1j0lfpg'] div[title='indValue']";

    public void waitForSalespipeLineAfterLogin() {
        waitForVisibleTextToAppear(salesPipeline);
    }

    public void navigateToSalesPipelinePage() {
        setImplicitTimeout(9, SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) (getDriver());
        js.executeScript("document.getElementsByClassName('css-u6v8er').item(1).lastElementChild.click();");
//		WebDriverWait wait = new WebDriverWait(getDriver(),60);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Sales Pipeline']//span[@class='css-132sc4d']")))
        waitForVisibleTextToAppear(addOpportunity);
        addOpportunity.click();
        waitForVisibleTextToAppear(insuranceTypePerson);
    }

    public void checkPersonTabState() {
        assertThat("true", equalTo(insuranceTypePerson.getAttribute("aria-selected")));
    }

    /*
     * Add policies indexes
     *  Auto -1
     *  Home -2
     *  Dwelling -3
     */
    public void fillTheOpportunityPersonalForm(String insuranceType, String fname, String lname, String phone, String email, String address,
                                               String addPolicies, String sourceName) {

        String url = pReader.getProperty("accountsURL", filename);
        getDriver().get(url);
        waitForVisibleTextToAppear(addOpportunity);
        addOpportunity.click();
        waitForVisibleTextToAppear(insuranceTypePerson);
        setImplicitTimeout(10, SECONDS);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) getDriver();
        firstName.sendKeys(fname);
        lastName.sendKeys(lname);
        phoneNumber.sendKeys(phone);
        emailId.sendKeys(email);
        contactAddress.click();
        contactAddress.type(address.trim());
        waitABit(10000);
        contactAddress.sendKeys(Keys.BACK_SPACE);
        addressDropdownlst.click();
        waitABit(3000);
        addPoliciesDropdown.click();
        String element;
        if (addPolicies.equalsIgnoreCase("one")) {
            element = dropdownValues.replace("index", "1");
            getDriver().findElement(By.cssSelector(element)).click();
        } else if (addPolicies.equalsIgnoreCase("two")) {
            element = dropdownValues.replace("index", "2");
            getDriver().findElement(By.cssSelector(element)).click();
        } else {
            element = dropdownValues.replace("index", "1");
            String element1 = dropdownValues.replace("index", "2");
            getDriver().findElement(By.cssSelector(element)).click();
            addPoliciesDropdown.click();
            waitABit(1000);
            getDriver().findElement(By.cssSelector(element1)).click();
        }
        waitABit(1000);
        String date = CommonUtils.getTheRequiredDate("MM/dd/YYYY");
        effectiveDate.type(date);
        sourceType.click();
        waitABit(1000);
        sourceTypelist.click();
        sName.type(sourceName);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(submitBtn));
        javascriptExecutor.executeScript("document.getElementsByClassName('css-1n66muu').item(0).click();");
        waitABit(1000);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(startBtn));
        startBtn.click();
    }

    public void fillTheOpportunityBusinessForm(String insuranceType, String fname, String lname, String phone, String email, String relationshipToBusiness,
                                               String businessName, String address, String industry, String addPolicies, String sourceName) {
        String url = pReader.getProperty("accountsURL", filename);
        getDriver().get(url);
        waitForVisibleTextToAppear(addOpportunity);
        addOpportunity.click();
        waitForVisibleTextToAppear(insuranceTypePerson);
        setImplicitTimeout(15, SECONDS);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) getDriver();
        insuranceBusinessType.click();
        waitForVisibleTextToAppear(businessNameTxt);
        firstName.sendKeys(fname);
        lastName.sendKeys(lname);
        phoneNumber.sendKeys(phone);
        emailId.sendKeys(email);
        relationshipToBusinessTxt.sendKeys(relationshipToBusiness);
        businessNameTxt.sendKeys(businessName);
        businessAddress.sendKeys(address.trim());
        waitABit(7000);
        businessAddress.sendKeys(Keys.BACK_SPACE);
        businessAddress.sendKeys(address.trim());
        waitABit(1000);
        addressDropdownlst.click();
        waitABit(3000);
        industryTxt.type(industry.trim());
        waitABit(3000);
        industryValue = industryValue.replace("indValue", industry.trim());
        getDriver().findElement(By.cssSelector(industryValue)).click();
        waitABit(6000);
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        wait.until(ExpectedConditions.elementToBeClickable(addPoliciesDropdown));
        addPoliciesdropdown.click();
        Select dropdownlst = new Select(addPolicieslstForBusiness);
        waitABit(3000);
        dropdownlst.selectByVisibleText(addPolicies);
        waitABit(1000);
        String date = CommonUtils.getTheRequiredDate("MM/dd/YYYY");
        effectiveDate.type(date);
        sourceType.click();
        waitABit(1000);
        sourceTypelist.click();
        sName.type(sourceName);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(submitBtn));
        javascriptExecutor.executeScript("document.getElementsByClassName('css-1n66muu').item(0).click();");
        waitABit(1000);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(startBtn));
        startBtn.click();
    }
}

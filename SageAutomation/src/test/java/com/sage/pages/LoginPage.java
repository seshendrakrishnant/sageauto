package com.sage.pages;

import com.sage.utilities.PropertyReader;
import com.sage.utilities.WebExtensibility;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import java.io.File;

public class LoginPage extends WebExtensibility {
	public static final String filename = System.getProperty("user.dir") + File.separator + "src" + File.separator
			+ "test" + File.separator + "resources" + File.separator + "test_Env.properties";
	PropertyReader pReader = new PropertyReader();
	String userName = pReader.getProperty("username", filename);
	String pwd = pReader.getProperty("password", filename);

	@FindBy(css = "input#email")
	WebElementFacade email;
	@FindBy(css = "input#password")
	WebElementFacade password;
	@FindBy(xpath = "//button[@type=\"submit\"]")
	WebElementFacade login;

	public void login() {
		String url = pReader.getProperty("webURL", filename);	
		launchBrowser(url);
		email.sendKeys(userName);
		password.sendKeys(pwd);
		login.click();		
	}

	public void closeBrowser() {
		getDriver().quit();
	}
}
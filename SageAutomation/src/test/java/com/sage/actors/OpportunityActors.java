package com.sage.actors;

import com.sage.pages.OpportunityPage;
import com.sage.pages.LoginPage;

import com.sage.pages.QuotingPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class OpportunityActors {

    @Steps
    LoginPage loginPage;
    OpportunityPage opportunityPage;
    QuotingPage quotingPage;

    @Step
    public void loginapplication() {
        loginPage.login();
    }

    @Step
    public void closeBrowser() {
        loginPage.closeBrowser();
    }

    @Step
    public void salespipelinelink() {
        opportunityPage.waitForSalespipeLineAfterLogin();
    }

    @Step
    public void clickOnSalesPipelineLink() {
        opportunityPage.navigateToSalesPipelinePage();
    }

    @Step
    public void verifyPersontabState() {
        opportunityPage.checkPersonTabState();
    }

    @Step
    public void fillTheOpportunityPersonalForm(String insuranceType, String fname, String lname, String phone, String email, String address, String addPolicies, String sourceName) {
        opportunityPage.fillTheOpportunityPersonalForm(insuranceType, fname, lname, phone, email, address, addPolicies, sourceName);
    }

    @Step
    public void fillTheOpportunityBusinessForm(String insuranceType, String fname, String lname, String phone, String email, String relationshipToBusiness, String businessName, String address, String industry, String addPolicies, String sourceName) {
        opportunityPage.fillTheOpportunityBusinessForm(insuranceType, fname, lname, phone, email, relationshipToBusiness,
                 businessName, address, industry, addPolicies,  sourceName);
    }

    @Step
    public void startQuoting() {
        quotingPage.startFillingQuotingForm();
    }
}

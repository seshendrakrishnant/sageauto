Feature: This feature file contains opportunity scenarios (Add opportunity)

  @sanity1
  Scenario: Verify login functionality
    Given user logs into sage application
    Then validate the login scenario

  @sanity1
  Scenario: Verify sales pipeline  and opporunity form with Person tab
    Given verify the sales pipeline link
    Then click on sales pipeline link
    And verify insurance is in selected state by default

  @sanity
  Scenario: Opportunity form filling for Personal.
    Given filling the opportunity form
#    And filling the quoting form

  @sanity1
  Scenario Outline: Opportunity form filling for Business.
    Given filling the opportunity form for business


  @sanity
  Scenario: close the sage application
    Given closing the browser
